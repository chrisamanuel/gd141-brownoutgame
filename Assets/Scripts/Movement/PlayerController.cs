﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Drag this script to any Unit that has a Movement
public class PlayerController : MonoBehaviour
{
    private Movement playerMovement;

    // Use this for initialization
    void Start()
    {
        playerMovement = GetComponent<Movement>();
    }

    // Update is called once per frame
    void Update()
    {
        playerMovement = GetComponent<Movement>();

        SetInput();

        ToggleWalk();
        CheckRequirements();
    }

    public void CheckRequirements()
    {
        if (!playerMovement)
        {
            Debug.Log(gameObject.name + " has no Movement");
        }
    }

    public void SetInput()
    {
        if (!playerMovement) return;

        playerMovement.InputX = Input.GetAxisRaw("Horizontal");
        playerMovement.InputY = Input.GetAxisRaw("Vertical");
    }

    public void Walk()
    {
        if (!playerMovement) return;
        playerMovement.CurrentMovementSpeed = playerMovement.WalkSpeed;
    }

    public void Run()
    {
        if (!playerMovement) return;
        playerMovement.CurrentMovementSpeed = playerMovement.DefaultMoveSpeed;
    }

    public void ToggleWalk()
    {
        if (!playerMovement) return;

        if (Input.GetKey(KeyCode.LeftShift))
        {
            Walk();
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            Run();
        }
    }


}
