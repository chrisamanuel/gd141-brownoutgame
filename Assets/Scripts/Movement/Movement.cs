﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public float DefaultMoveSpeed;
    public float CurrentMovementSpeed;
    public float WalkSpeed;
    public float InputX;
    public float InputY;

    private Rigidbody2D myRigidBody;

    // Use this for initialization
    void Start()
    {
        myRigidBody = GetComponent<Rigidbody2D>();
        WalkSpeed = DefaultMoveSpeed / 1.5f;
        CurrentMovementSpeed = DefaultMoveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        myRigidBody = GetComponent<Rigidbody2D>();

        Move();

        CheckRequirements();
    }

    public void CheckRequirements()
    {
        if (!myRigidBody)
        {
            Debug.Log(gameObject.name + " has no Rigidbody2D attached!");
        }

        if(DefaultMoveSpeed == 0)
        {
            Debug.Log(gameObject.name + "'s default movement speed is not set.");
        }
    }

    public void Move()
    {
        if (!myRigidBody) return;

        if (InputX != 0 || InputY !=0)
        {
            Vector2 MovementVector = new Vector2(InputX, InputY);
            MovementVector.Normalize();
            myRigidBody.velocity = (MovementVector * CurrentMovementSpeed * Time.deltaTime);
        }
        else if(InputX == 0 && InputY == 0)
        {
            StopMovement();
        }
    }

    public void StopMovement()
    {
        myRigidBody.velocity = new Vector2(0f, 0f);
    }
}
