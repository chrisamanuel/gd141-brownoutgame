﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlugRouter : Task
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerController>() && !Done)
        {
            Done = true;
            Debug.Log(other.GetComponent<Unit>().Name + " turned on the Wi-Fi Router.");
        }
    }
}
