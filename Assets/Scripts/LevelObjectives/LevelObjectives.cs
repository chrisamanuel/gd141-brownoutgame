﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelObjectives : MonoBehaviour
{
    public bool LevelComplete;
    public bool ItemsComplete;
    public bool TasksAllDone;

    public List<Unit> Players = new List<Unit>();

    public List<Item> ItemsToFind = new List<Item>();
    public List<Task> TasksToDo = new List<Task>();

    // Use this for initialization
    void Start()
    {
        ItemsComplete = false;
        TasksAllDone = false;
        EchoObjectives();

        if(ItemsToFind.Count <= 0)
        {
            Debug.Log("There are no items to find. Please set it on " + gameObject.name + " before the start of the game.");
        }

        if (TasksToDo.Count <= 0)
        {
            Debug.Log("There are no tasks to do. Please set it on " + gameObject.name + " before the start of the game if there are any tasks.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        CheckRequirements();
        CheckPlayerItems();
        CheckTaskStatus();
        CheckProgress();
    }

    public void CheckRequirements()
    {
        if (Players.Count <= 0)
        {
            Debug.Log("Please set the players in the " + gameObject.name + "!");
        }
    }

    //DISPLAY OBJECTIVES IN CONSOLE
    public void EchoObjectives()
    { 
        foreach (Unit player in Players)
        {
            Debug.Log("Player " + player.Name + " connected.");
        }

        foreach(Item requirement in ItemsToFind)
        {
            Debug.Log("Find : " + requirement.Name);
        }

        foreach(Task requirement in TasksToDo)
        {
            Debug.Log("Task : " + requirement.Description);
        }
    }

    public void CheckProgress()
    {
        if (ItemsToFind.Count == 0 && TasksToDo.Count == 0)
        {
            LevelComplete = true;
        }

        if (ItemsToFind.Count == 0)
        {
            ItemsComplete = true;
        }

        if (TasksToDo.Count == 0)
        {
            TasksAllDone = true;
        }
    }

    public void CheckPlayerItems()
    {
        if (Players.Count <= 0) return;

        foreach (Unit player in Players)
        {
            foreach (Item Requirement in ItemsToFind.ToList())
            {
                if (player.GetComponent<Inventory>().Items.Contains(Requirement))
                {
                    Debug.Log(player.Name + " found " + Requirement.Name + "!");
                    ItemsToFind.Remove(Requirement);
                }
            }
        }
    }

    public void CheckTaskStatus()
    {
        foreach (Task Requirement in TasksToDo.ToList())
        {
            if (Requirement.Done)
            {
                TasksToDo.Remove(Requirement);
            }
        }
    }

}
