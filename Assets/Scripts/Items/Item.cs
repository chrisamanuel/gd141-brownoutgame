﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : Pickupable
{
    public string Name;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Inventory>())
        {
            transform.position = new Vector3(100, 100, 100);
            other.GetComponent<Inventory>().Items.Add(this);
        }
    }
}
