﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Furniture : MonoBehaviour
{
    public string Name;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Unit>())
        {
            Debug.Log(other.GetComponent<Unit>().Name + " bumped into a " + Name + "!");
        }
    }
}
