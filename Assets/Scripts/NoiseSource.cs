﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseSource : MonoBehaviour
{
    public float NoiseValue;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Unit>())
        { 
            Debug.Log("Added " + NoiseValue + " to NOISE METER.");
        }
    }
}
